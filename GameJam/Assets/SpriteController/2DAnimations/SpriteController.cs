﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CharacterController)), RequireComponent(typeof(Animator))]
public class SpriteController : MonoBehaviour 
{

	CharacterController _controller;

	public bool IsControllable{get; private set;}

	Animator _anitmator;

	float time = 0.1f;

	// Use this for initialization
	void Start () 
	{
		IsControllable = true;
		_controller = GetComponent<CharacterController>();
		_anitmator = GetComponent<Animator>();
	}

	public void finishMove()
	{
		Debug.Log("Finish move");
		_anitmator.SetBool("WalkingRight", false);
		_anitmator.SetBool("WalkingLeft", false);
		_anitmator.SetBool("WalkingUp", false);
		_anitmator.SetBool("WalkingDown", false);
		IsControllable = true;
	}

	public void MoveTo(Vector2 pos)
	{
		transform.position = new Vector3(pos.x, pos.y, -2);
	}

	// Update is called once per frame


	public void MoveRight(int noBlocks)
	{
		IsControllable = false;
		Debug.Log("Right motherfucker!");

		Hashtable args = new Hashtable();
		args["time"] = time;
		args["amount"] = new Vector3(noBlocks * 1,0,0);
		args["easeyype"] = iTween.EaseType.easeInCirc;
		args["oncomplete"] = "finishMove";
		args["oncompletetarget"]=gameObject;
		iTween.MoveBy(gameObject, args);
		//iTween.MoveBy(gameObject, new Vector3(1,0,0), 3.0f);
		_anitmator.SetBool("WalkingRight", true);
	}

	public void MoveLeft(int noBlocks)
	{
	IsControllable = false;
	Debug.Log("Left motherfucker!");
		
		Hashtable args = new Hashtable();
		args["time"] = time;
		args["amount"] = new Vector3(noBlocks * -1,0,0);
		args["easeyype"] = iTween.EaseType.easeInCirc;
		args["oncomplete"] = "finishMove";
		args["oncompletetarget"]=gameObject;
		iTween.MoveBy(gameObject, args);

		_anitmator.SetBool("WalkingLeft", true);
	}

	public void MoveUp(int noBlocks)
	{
	IsControllable = false;
	Debug.Log("Up motherfucker!");
		
		Hashtable args = new Hashtable();
		args["time"] = time;
		args["amount"] = new Vector3(0,noBlocks * 1,0);
		args["easeyype"] = iTween.EaseType.easeInCirc;
		args["oncomplete"] = "finishMove";
		args["oncompletetarget"]=gameObject;
		iTween.MoveBy(gameObject, args);

		_anitmator.SetBool("WalkingUp", true);
	}

	public void MoveDown(int noBlocks)
	{
		IsControllable = false;
		Debug.Log("Down motherfucker!");
		
		Hashtable args = new Hashtable();
		args["time"] = time;
		args["amount"] = new Vector3(0,noBlocks * -1,0);
		args["easeyype"] = iTween.EaseType.easeInCirc;
		args["oncomplete"] = "finishMove";
		args["oncompletetarget"]=gameObject;
		iTween.MoveBy(gameObject, args);

		_anitmator.SetBool("WalkingDown", true);
	}

}
