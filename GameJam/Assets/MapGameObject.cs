﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class MapGameObject : MonoBehaviour 
{
	public int _nextLevel;

	public int _damage;


	int _steps;
	public Text _stepCounter;



	public float shadowMoveAngle;
	public float sheepMoveAngle;

	public int dimX;
	public int dimY;

	public Vector2 charStartPos;
	public Vector2 sheepStartPos;
	public Vector2 shadowStartPos;
	public Vector2 endPos;

	public Vector2 charPos;
	public Vector2 shadowPos;
	public Vector2 sheepPos;

	public GameObject character;
	public GameObject Sheep;
	public GameObject shadow;
	public GameObject end;

	public AudioSource exitSE;
	public AudioSource portalSE;
	public AudioSource sheepSE;
	public AudioSource munchSE;
	public AudioSource keySE;

	public SpriteController characterController;
	public SpriteController shadowController;
	public SpriteController sheepController;

	public int blockSize;
	public GameObject _templateBlock;
	public Sprite teleportInSprite;
	public Sprite teleportOutSprite;
	public GameObject _templateWallUp;
	public GameObject _templateWallRight;
	public GameObject _templateWallDown;
	public GameObject _templateWallLeft;
	public GameObject _templateGateUp;
	public GameObject _templateGateRight;
	public GameObject _templateGateDown;
	public GameObject _templateGateLeft;
	public GameObject _templateKey;
	public GameObject _templateSwitch;
	public GameObject _templateIce;
	public GameObject _templateCivillian;

	public Camera _gameCamera;

	bool allowInput = true;

	Vector2 _charMoveDir = new Vector2(1,0);
	Vector2 _shadowMoveDir = new Vector2(1,0);
	Vector2 _sheepMoveDir = new Vector2(1,0);

	public Vector3[] walls;
	public Vector2[] teleportFrom;
	public Vector2[] teleportTo;
    public List<Vector2> keys;
    public List<Vector3> gates;

	public List<Vector2> _switches;
	public List<Vector2> _civillians;

	public List<Vector2> _ice;

	private List<GameObject> _keyObjs;
	private List<GameObject> _gateObjs;
	private List<GameObject> _switchObjs;
	private List<GameObject> _civillianObjs;



	Map _map;

	// Use this for initialization
	void Awake () 
	{
		_steps = 0;
		_map = new Map(dimX, dimY, blockSize, walls, Vector2.zero, Vector2.one, new Vector2(dimX, dimY), 
		               _templateBlock, _templateWallUp, _templateWallRight, _templateWallDown, _templateWallLeft, gameObject);
		float camPoxX = Mathf.Ceil(((float)dimX)/2.0f) - 1;
		float camPoxY = Mathf.Ceil(((float)dimY)/2.0f) - 1;
		_gameCamera.transform.position = new Vector3( camPoxX, camPoxY, -10);
		_gameCamera.orthographicSize = dimX / 2 + 1;
		if (dimY > dimX)_gameCamera.orthographicSize = dimY / 2 + 1;

		character.transform.position = new Vector3(charStartPos.x, charStartPos.y, -2);
		shadow.transform.position = new Vector3(shadowStartPos.x, shadowStartPos.y, -2);

		end.transform.position = new Vector3(endPos.x, endPos.y, -2);

		characterController = character.GetComponent<SpriteController>();
		shadowController = shadow.GetComponent<SpriteController>();
		if(Sheep!=null)
		{
			sheepController = Sheep.GetComponent<SpriteController>();
			Sheep.transform.position = new Vector3(sheepStartPos.x, sheepStartPos.y, -2);
		}

		charPos = charStartPos;
		shadowPos = shadowStartPos;
		sheepPos = sheepStartPos;



		_templateBlock.SetActive(false);
		_templateWallUp.SetActive(false);
		_templateWallLeft.SetActive(false);
		_templateWallDown.SetActive(false);
		_templateWallRight.SetActive(false);


		//set teleport
		for ( int i = 0 ; i < teleportFrom.Length ; i++ )
		{
			int fromX = (int)teleportFrom[i].x;
			int fromY = (int)teleportFrom[i].y;

			int toX = (int)teleportTo[i].x;
			int toY = (int)teleportTo[i].y;

			_map.blocks[fromX, fromY]._blockType = BlockType.TeleportIn;
			_map.blocks[fromX, fromY]._teleportTo = teleportTo[i];
			_map.blocks[fromX, fromY].gameObject.GetComponent<SpriteRenderer>().sprite = teleportInSprite;

			_map.blocks[toX, toY]._blockType = BlockType.TeleportOut;
			_map.blocks[toX, toY].gameObject.GetComponent<SpriteRenderer>().sprite = teleportOutSprite;
		}
        
		for ( int i = 0 ; i < _ice.Count ; i++)
		{
			_map.blocks[(int)_ice[i].x,(int)_ice[i].y]._blockType = BlockType.Ice;
			GameObject iceObj = (GameObject)Instantiate(_templateIce);
			iceObj.transform.position = new Vector3((int)_ice[i].x,(int)_ice[i].y, -1);
			iceObj.SetActive(true);
			iceObj.transform.parent = gameObject.transform;
		}

		_gateObjs = new List<GameObject>();
		_keyObjs = new List<GameObject>();

		foreach (Vector2 key in keys)
		{
			GameObject keyObj = (GameObject)Instantiate(_templateKey);
			keyObj.transform.position = new Vector3(key.x, key.y, -1);
			keyObj.SetActive(true);
			_keyObjs.Add(keyObj);
			keyObj.transform.parent = gameObject.transform;
		}

		_switchObjs = new List<GameObject>();
		foreach (Vector2 angleSwitch in _switches)
		{
			GameObject angleSwitchObj = (GameObject)Instantiate(_templateSwitch);
			angleSwitchObj.transform.position = new Vector3(angleSwitch.x, angleSwitch.y, -1);
			angleSwitchObj.SetActive(true);
			_switchObjs.Add(angleSwitchObj);
			angleSwitchObj.transform.parent = gameObject.transform;
		}

		_civillianObjs = new List<GameObject>();
		foreach (Vector2 angleCivillian in _civillians)
		{
			GameObject angleCivillianObj = (GameObject)Instantiate(_templateCivillian);
			angleCivillianObj.transform.position = new Vector3(angleCivillian.x, angleCivillian.y, -1);
			angleCivillianObj.SetActive(true);
			_civillianObjs.Add(angleCivillianObj);
			angleCivillianObj.transform.parent = gameObject.transform;
		}

		foreach (Vector3 gate in gates)
		{
			int dir = (int)gate.z;
			GameObject gateObj = null;

			switch(dir)
			{
			case 0:
				gateObj = (GameObject)Instantiate(_templateGateUp);
				CreateGate(gate);
				break;
			case 1:
				gateObj = (GameObject)Instantiate(_templateGateRight);
				CreateGate(gate);
				break;
			case 2:
				gateObj = (GameObject)Instantiate(_templateGateDown);
				CreateGate(gate);
				break;
			case 3:
				gateObj = (GameObject)Instantiate(_templateGateLeft);
				CreateGate(gate);
				break;
			}

			gateObj.transform.position = new Vector3(gate.x, gate.y, -1);
			gateObj.SetActive(true);
			_gateObjs.Add(gateObj);
		}

	}

	Vector2 RotateVec2D(Vector2 vec, float angle)
	{
		Vector2 rotatedVec = new Vector2
			(
				(int)(vec.x * Mathf.Cos(Mathf.Deg2Rad*angle) - vec.y*Mathf.Sin(Mathf.Deg2Rad*angle)),
				(int)(-1*(vec.x * Mathf.Sin(Mathf.Deg2Rad*angle) - vec.y*Mathf.Cos(Mathf.Deg2Rad*angle)))
			);

		return rotatedVec;
	}

	public void ResetLevel()
	{

		charPos = charStartPos;
		shadowPos = shadowStartPos;
		sheepPos = sheepStartPos;

		character.transform.position = new Vector3(charStartPos.x, charStartPos.y, -2);
		shadow.transform.position = new Vector3(shadowStartPos.x, shadowStartPos.y, -2);
		Sheep.transform.position = new Vector3(sheepStartPos.x, sheepStartPos.y, -2);
		end.transform.position = new Vector3(endPos.x, endPos.y, -2);
	}

	bool CanMoveLeft(Vector2 pos)
	{
		return (_map.blocks[(int)pos.x, (int)pos.y].blockLeft != null);
	}

	bool CanMoveRight(Vector2 pos)
	{
		return (_map.blocks[(int)pos.x, (int)pos.y].blockRight != null);
	}

	bool CanMoveUp(Vector2 pos)
	{
		return (_map.blocks[(int)pos.x, (int)pos.y].blockUp != null);
	}

	bool CanMoveDown(Vector2 pos)
	{
		Block fromBlock = _map.blocks[(int)pos.x, (int)pos.y];
		Block moveTo = fromBlock.blockDown;
		bool canMove = moveTo != null;
		return moveTo;
		//return (_map.blocks[(int)pos.x, (int)pos.y].blockDown != null);
	}

	void MoveShadow(Vector2 shadowMove)
	{
		bool moving = false;
		if (shadowMove.x == 1 && CanMoveRight(shadowPos)){ moving = true; shadowController.MoveRight(1);}
		else if (shadowMove.x == -1 && CanMoveLeft(shadowPos)){ moving = true; shadowController.MoveLeft(1);}
		else if (shadowMove.y == 1 && CanMoveUp(shadowPos)){ moving = true; shadowController.MoveUp(1);}
		else if (shadowMove.y == -1 && CanMoveDown(shadowPos)){ moving = true; shadowController.MoveDown(1);}

		if (!moving)_shadowMoveDir = Vector2.zero;
	}

	void MoveSheep(Vector2 sheepMove)
	{
		if (Sheep == null ) return;
		bool moving = false;
		if (sheepMove.x == 1 && CanMoveRight(sheepPos)){ moving = true; sheepController.MoveRight(1);}
		else if (sheepMove.x == -1 && CanMoveLeft(sheepPos)){ moving = true; sheepController.MoveLeft(1);}
		else if (sheepMove.y == 1 && CanMoveUp(sheepPos)){ moving = true; sheepController.MoveUp(1);}
		else if (sheepMove.y == -1 && CanMoveDown(sheepPos)){ moving = true; sheepController.MoveDown(1);}
		
		if (!moving)_sheepMoveDir = Vector2.zero;
	}

	void CheckGameOver()
	{
		if ((charPos == shadowPos) && (charPos == endPos))
		{
			Debug.LogWarning("Praise the sun !");
			exitSE.Play();
			FindObjectOfType<Manager>().ChangeLevel(_nextLevel);
			return;
		}

		if (charPos == shadowPos)
		{
			Debug.LogWarning("Shadow Has Consumed You");
			munchSE.Play();
			FindObjectOfType<Manager>().doEnding(Manager.Ending.shadow);
			return;
		}

		if (sheepController!=null && charPos == sheepPos)
		{
			Debug.LogWarning("Sheep Has Consumed You");
			sheepSE.Play();
			FindObjectOfType<Manager>().doEnding(Manager.Ending.sheep);
			return;
		}

		if (charPos == endPos)
		{
			Debug.LogWarning("You exited alone");
			sheepSE.Play();
			FindObjectOfType<Manager>().GameOver();
			return;
		}

		if (shadowPos == endPos)
		{
			Debug.LogWarning("The shadow escaped");
			sheepSE.Play();
			FindObjectOfType<Manager>().GameOver();
			return;
		}
	}

	void CheckTeleport()
	{
		Block curCharBlock = _map.blocks[(int)charPos.x, (int)charPos.y];
		Block curShadowBlock = _map.blocks[(int)shadowPos.x, (int)shadowPos.y];

		if (curCharBlock._blockType == BlockType.TeleportIn)
		{
			portalSE.Play();
			charPos = curCharBlock._teleportTo;
			characterController.MoveTo(charPos);
		}

		if (curShadowBlock._blockType == BlockType.TeleportIn)
		{
			portalSE.Play();
			shadowPos = curShadowBlock._teleportTo;
			shadowController.MoveTo(shadowPos);
		}
	}

    void CheckKey()
    {
        Block curCharBlock = _map.blocks[(int)charPos.x, (int)charPos.y];

        for (int i = 0; i < keys.Count; i++)
        {
            if ((int)charPos.x == (int)keys[i].x && (int)charPos.y == (int)keys[i].y)
            {
				keySE.Play();
                OpenGate(i);
                keys.RemoveAt(i);
                gates.RemoveAt(i);
                break;
            }
        }
    }

	void CheckSwitch()
	{
		Block curCharBlock = _map.blocks[(int)charPos.x, (int)charPos.y];
		
		for (int i = 0; i < _switches.Count; i++)
		{
			if ((int)charPos.x == (int)_switches[i].x && (int)charPos.y == (int)_switches[i].y)
			{
				shadowMoveAngle -= 90;
				_switches.RemoveAt(i);
				Destroy(_switchObjs[i]);
				_switchObjs.RemoveAt(i);
				break;
			}
		}
	}

	void CheckCivillian()
	{
		//Block curCharBlock = _map.blocks[(int)shadowPos.x, (int)shadowPos.y];
		
		for (int i = 0; i < _civillians.Count; i++)
		{
			if ((int)shadowPos.x == (int)_civillians[i].x && (int)shadowPos.y == (int)_civillians[i].y)
			{
				munchSE.Play();
				_civillians.RemoveAt(i);
				Destroy(_civillianObjs[i]);
				_civillianObjs.RemoveAt(i);
				FindObjectOfType<Manager>().EatCiv();
				break;
			}
		}

	}

    private void OpenGate(int i)
    {
		//clear key
		Destroy(_keyObjs[i]);
		_keyObjs.RemoveAt(i);

		//clear gate
		Destroy(_gateObjs[i]);
		_gateObjs.RemoveAt(i);

        Block gateBlock = _map.blocks[(int)gates[i].x, (int)gates[i].y];

        int dir = (int)gates[i].z;

        switch(dir)
        {
            case 0:
                gateBlock.blockUp = _map.blocks[(int)gates[i].x, (int)gates[i].y + 1];
                _map.blocks[(int)gates[i].x, (int)gates[i].y + 1].blockDown = gateBlock;
                break;
            case 1:
                gateBlock.blockRight = _map.blocks[(int)gates[i].x + 1, (int)gates[i].y];
                _map.blocks[(int)gates[i].x + 1, (int)gates[i].y].blockLeft = gateBlock;
                break;
            case 2:
                gateBlock.blockDown = _map.blocks[(int)gates[i].x, (int)gates[i].y - 1];
                _map.blocks[(int)gates[i].x, (int)gates[i].y - 1].blockUp = gateBlock;
                break;
            case 3:
                gateBlock.blockLeft = _map.blocks[(int)gates[i].x - 1, (int)gates[i].y];
                _map.blocks[(int)gates[i].x - 1, (int)gates[i].y].blockRight = gateBlock;
                break;
            default:
                break;
        }
    }

	private void CreateGate(Vector3 gate)
	{
		Block gateBlock = _map.blocks[(int)gate.x, (int)gate.y];
		
		int dir = (int)gate.z;
		
		switch(dir)
		{
		case 0:
			_map.blocks[(int)gate.x, (int)gate.y + 1].blockDown = null;
			gateBlock.blockUp = null;
			break;
		case 1:
			_map.blocks[(int)gate.x + 1, (int)gate.y].blockLeft = null;
			gateBlock.blockRight = null;
			break;
		case 2:
			_map.blocks[(int)gate.x, (int)gate.y - 1].blockUp = null;
			gateBlock.blockDown = null;
			break;
		case 3:
			_map.blocks[(int)gate.x - 1, (int)gate.y].blockRight = null;
			gateBlock.blockLeft = null;
			break;
		default:
			break;
		}
	}

	void IncrementSteps()
	{
		_steps++;
		_stepCounter.text = _steps.ToString();
	}

	int GetMoveStepsRight(int curX, int curY)
	{
		int steps = 0;
		Block curBlock = _map.blocks[curX, curY];
		if (curBlock.blockRight!=null)steps++;
		else return steps;

		curBlock = curBlock.blockRight;

		while (curBlock.blockRight != null && curBlock._blockType == BlockType.Ice)
		{
			steps++;
			curBlock = curBlock.blockRight;
		}

		return steps;
	}

	int GetMoveStepsLeft(int curX, int curY)
	{
		int steps = 0;
		Block curBlock = _map.blocks[curX, curY];
		if (curBlock.blockLeft!=null)steps++;
		else return steps;
		
		curBlock = curBlock.blockLeft;
		
		while (curBlock.blockLeft != null && curBlock._blockType == BlockType.Ice)
		{
			steps++;
			curBlock = curBlock.blockLeft;
		}
		
		return steps;
	}

	int GetMoveStepsUp(int curX, int curY)
	{
		int steps = 0;
		Block curBlock = _map.blocks[curX, curY];
		if (curBlock.blockUp!=null)steps++;
		else return steps;
		
		curBlock = curBlock.blockUp;
		
		while (curBlock.blockUp != null && curBlock._blockType == BlockType.Ice)
		{
			steps++;
			curBlock = curBlock.blockUp;
		}
		
		return steps;
	}

	int GetMoveStepsDown(int curX, int curY)
	{
		int steps = 0;
		Block curBlock = _map.blocks[curX, curY];
		if (curBlock.blockDown!=null)steps++;
		else return steps;
		
		curBlock = curBlock.blockDown;
		
		while (curBlock.blockDown != null && curBlock._blockType == BlockType.Ice)
		{
			steps++;
			curBlock = curBlock.blockDown;
		}
		
		return steps;
	}

	int GetMoveSteps(int moveDir)
	{
		int curX = (int)charPos.x;
		int curY = (int)charPos.y;

		int steps = 0;

		switch(moveDir)
		{
		case 0:
			steps = GetMoveStepsUp(curX, curY);
			break;
		case 1:
			steps = GetMoveStepsRight(curX, curY);
			break;
		case 2:
			steps = GetMoveStepsDown(curX, curY);
			break;
		case 3:
			steps = GetMoveStepsLeft(curX, curY);
			break;
		}

		return steps;
	}

	// Update is called once per frame
	void Update () 
	{

		if (!allowInput)
		{
			allowInput = characterController.IsControllable && shadowController.IsControllable;

			if (allowInput)
			{
				charPos += _charMoveDir;
				shadowPos = new Vector2((int)shadowPos.x + (int)_shadowMoveDir.x, (int)shadowPos.y + (int)_shadowMoveDir.y);
				sheepPos = new Vector2((int)sheepPos.x + (int)_sheepMoveDir.x, (int)sheepPos.y + (int)_sheepMoveDir.y);

				Debug.Log("Char pos = " + charPos.ToString());
				Debug.Log("Shadow pos = " + shadowPos.ToString());
				Debug.Log("Sheep pos = " + sheepPos.ToString());

				if (sheepPos == shadowPos)
				{
					if (sheepSE!=null && sheepController != null)
						sheepSE.Play();
					sheepMoveAngle = 90;
				}

				CheckTeleport();
				CheckKey();
				CheckSwitch();
				CheckCivillian();
				CheckGameOver();

			}
			return;
		}
		_charMoveDir = Vector2.zero;
		if (Input.GetButtonDown("MoveRight") && CanMoveRight(charPos))
		{
			IncrementSteps();
			_charMoveDir = new Vector2(1,0);

			allowInput = false;
			Debug.Log("Right !");

			int charMoveSteps = GetMoveSteps(1);
			_shadowMoveDir = RotateVec2D(_charMoveDir, shadowMoveAngle);
			_sheepMoveDir = RotateVec2D(_charMoveDir, sheepMoveAngle);

			_charMoveDir*=charMoveSteps;
			characterController.MoveRight(charMoveSteps);

			MoveShadow(_shadowMoveDir);
			MoveSheep(_sheepMoveDir);
		}
		
		else if (Input.GetButtonDown("MoveLeft") && CanMoveLeft(charPos))
		{
			IncrementSteps();
			_charMoveDir = new Vector2(-1,0);
			allowInput = false;
			Debug.Log("Left !");

			int charMoveSteps = GetMoveSteps(3);


			_shadowMoveDir = RotateVec2D(_charMoveDir, shadowMoveAngle);
			_sheepMoveDir = RotateVec2D(_charMoveDir, sheepMoveAngle);

			_charMoveDir*=charMoveSteps;
			characterController.MoveLeft(charMoveSteps);

			MoveShadow(_shadowMoveDir);
			MoveSheep(_sheepMoveDir);
		}
		
		else if (Input.GetButtonDown("MoveUp") && CanMoveUp(charPos))
		{
			IncrementSteps();
			_charMoveDir = new Vector2(0,1);
			allowInput = false;
			Debug.Log("Up !");

			int charMoveSteps = GetMoveSteps(0);

			_shadowMoveDir = RotateVec2D(_charMoveDir, shadowMoveAngle);
			_sheepMoveDir = RotateVec2D(_charMoveDir, sheepMoveAngle);

			_charMoveDir*=charMoveSteps;
			characterController.MoveUp(charMoveSteps);

			MoveShadow(_shadowMoveDir);
			MoveSheep(_sheepMoveDir);
		}
		
		else if (Input.GetButtonDown("MoveDown") && CanMoveDown(charPos))
		{
			IncrementSteps();
			_charMoveDir = new Vector2(0,-1);
			allowInput = false;
			Debug.Log("Down !");

			int charMoveSteps = GetMoveSteps(2);

			_shadowMoveDir = RotateVec2D(_charMoveDir, shadowMoveAngle);
			_sheepMoveDir = RotateVec2D(_charMoveDir, sheepMoveAngle);

			_charMoveDir*=charMoveSteps;
			characterController.MoveDown(charMoveSteps);

			MoveShadow(_shadowMoveDir);
			MoveSheep(_sheepMoveDir);
		}
	}	

}


