﻿using UnityEngine;
using System.Collections;

public enum BlockType
{
	Normal,
	TeleportIn,
	TeleportOut,
	Ice,
}

public class Block : MonoBehaviour
{
	internal Block blockLeft = null;
	internal Block blockRight = null;
	internal Block blockUp = null;
	internal Block blockDown = null;

	public Vector2 _teleportTo = Vector2.zero;
	public BlockType _blockType = BlockType.Normal;

	public Sprite _background;

	public Block()
	{
	}
}
