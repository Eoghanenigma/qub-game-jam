﻿using UnityEngine;
using System.Collections;

public class SlideRunner : MonoBehaviour {

	public GameObject[] _slides;
	public float[] _slideTimings;

	// Use this for initialization
	void Start () 
	{
		StartCoroutine(RunSlides());
	}

	IEnumerator RunSlides()
	{
		for ( int i = 0 ; i < _slides.Length ; i++ )
		{
			_slides[i].SetActive(true);
			yield return new WaitForSeconds(_slideTimings[i]);
			_slides[i].SetActive(false);
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}
}
