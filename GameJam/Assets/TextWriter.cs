﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TextWriter : MonoBehaviour {

	public Text _text;
	string _string;

	void Awake ()
	{
		_string = _text.text;
		_text.text = "";
	}

	// Use this for initialization
	void Start () {
		StartText();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator DrawText()
	{
		int chars = 0;
		while (chars < _string.Length)
		{
			chars++;
			_text.text = _string.Substring(0,chars);
			yield return new WaitForSeconds(.1f);
		}
	}

	public void StartText()
	{
		StartCoroutine(DrawText());
	}
}
