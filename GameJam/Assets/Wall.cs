﻿using UnityEngine;
using System.Collections;

public struct Wall
{
	public int posX;
	public int posY;

	public int dir;

}
