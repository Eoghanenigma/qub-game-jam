﻿using UnityEngine;
using System.Collections;

public class Map 
{
	public Block CharacterBlock;
	public Block ShadowBlock;
	public Block EndBlock;



	public Block[,] blocks;

	int _dimX;
	int _dimY;

	int _blockSize;

	public Map(int dimX, int dimY, int blockSize, Vector3[] walls, Vector2 charStart, Vector2 shadowStart, Vector2 endPos, 
	           GameObject templateBlock, GameObject wallUp, GameObject wallRight, GameObject wallDown, GameObject wallLeft, GameObject parent)	
	{
		_dimX = dimX;
		_dimY = dimY;
		blocks = new Block[dimX, dimY];

		//create blocks
		for(int x  = 0  ; x < _dimX ; x++ )
		{
			for(int y  = 0  ; y < _dimY ; y++ )
			{
				GameObject blockInstance = (GameObject)GameObject.Instantiate(templateBlock);
				blockInstance.transform.parent = parent.transform;
				blocks[x,y] = blockInstance.GetComponent<Block>();

				blocks[x,y].transform.position = new Vector3(x*blockSize,y*blockSize, 0);
			}		
		}

		//link blocks
		for(int x  = 0  ; x < _dimX ; x++ )
		{
			for(int y  = 0  ; y < _dimY ; y++ )
			{
				if ( (x - 1) >= 0 ) blocks[x,y].blockLeft = blocks[x-1,y];
				if ( (x + 1) < dimX ) blocks[x,y].blockRight = blocks[x+1,y];
				if ( (y - 1) >= 0 ) blocks[x,y].blockDown = blocks[x,y-1];
				if ( (y + 1) < _dimY ) blocks[x,y].blockUp = blocks[x,y+1];
			}		
		}

		//make walls
		foreach (Vector3 wall in walls)
		{
			int x = (int)wall.x;
			int y = (int)wall.y;

			Block block1 = blocks[x,y];
			Block block2;

			switch((int)wall.z)
			{
			case 0: //up
				block2 = block1.blockUp;
				block1.blockUp = null;
				block2.blockDown = null;
				GameObject wallUpInst = (GameObject)GameObject.Instantiate(wallUp);				
				wallUpInst.transform.parent = parent.transform;
				wallUpInst.transform.position = new Vector3(x*blockSize,y*blockSize, wallUpInst.transform.position.z);
				break;
			case 1: // right
				block2 = block1.blockRight;
				block1.blockRight = null;
				block2.blockLeft = null;
				GameObject wallRightInst = (GameObject)GameObject.Instantiate(wallRight);				
				wallRightInst.transform.parent = parent.transform;
				wallRightInst.transform.position = new Vector3(x*blockSize,y*blockSize, wallRightInst.transform.position.z);
				break;
			case 2: //down
				block2 = block1.blockDown;
				block1.blockDown = null;
				block2.blockUp = null;
				GameObject wallDownInst = (GameObject)GameObject.Instantiate(wallDown);				
				wallDownInst.transform.parent = parent.transform;
				wallDownInst.transform.position = new Vector3(x*blockSize,y*blockSize, wallDownInst.transform.position.z);
				break;
			case 3: //left
				block2 = block1.blockLeft;
				block1.blockLeft = null;
				block2.blockRight = null;
				GameObject wallLeftInst = (GameObject)GameObject.Instantiate(wallLeft);				
				wallLeftInst.transform.parent = parent.transform;
				wallLeftInst.transform.position = new Vector3(x*blockSize,y*blockSize, wallLeftInst.transform.position.z);
				break;			
			}
		}
	}
}
