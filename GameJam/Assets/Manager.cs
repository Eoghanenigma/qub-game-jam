﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Manager : MonoBehaviour {

	public GameObject[] levels;
	public Image _displayImage;
	public Sprite[] sprites;
	public Sprite gameOverImage;
	public Sprite _eatCivSprite;
	public AudioSource sbsong;
	public AudioSource endsong;

	bool _civilianEaten = false;

	public GameObject _goodEnding;
	public GameObject _badEnding;
	public GameObject _shadowEnding;
	public GameObject _sheepEnding;

	public enum Ending {
		good,
		bad,
		shadow,
		sheep
	}

	void Start()
	{
		ChangeLevel(0);
	}

	public void GameOver()
	{
		_displayImage.enabled = true;
		_displayImage.sprite = gameOverImage;
	}

	public void BadEnding()
	{
		_badEnding.SetActive(true);
	}

	public void GoodEnding()
	{
		_goodEnding.SetActive(true);
	}

	public void ShadowEnding()
	{
		_shadowEnding.SetActive(true);
	}

	public void SheepEnding()
	{
		_sheepEnding.SetActive(true);
	}

	IEnumerator EndingMusic()
	{
		sbsong.Stop();
		yield return null; 
		endsong.Play (); 
	}

	public void doEnding(Ending end)
	{
		StartCoroutine(EndingMusic());

		switch (end)
		{
		case Ending.good:
			GoodEnding ();
			break;
		case Ending.bad:
			BadEnding ();
			break;
		case Ending.shadow:	
			ShadowEnding ();
			break;
		case Ending.sheep:
			SheepEnding ();
			break;
		}
	}

	public void EatCiv()
	{
		_civilianEaten = true;
	}

	public void ChangeLevel(int level)
	{
		foreach(GameObject go in levels)
		{
			go.SetActive(false);
		}

		if (level !=-1)
		{
			_displayImage.enabled = true;
			_displayImage.sprite = sprites[level];

			if (level == 2 && _civilianEaten)
				_displayImage.sprite = _eatCivSprite;

			//levels[level].GetComponentInChildren<MapGameObject>().ResetLevel();
			StartCoroutine("ChangeTheLevel", level);
			levels[level].SetActive(true);
		}
		else
		{
			if(_civilianEaten)doEnding(Ending.bad);
			else doEnding(Ending.good);
		}
	}

	IEnumerator ChangeTheLevel(int level)
	{
		yield return new WaitForSeconds(5);
		levels[level].SetActive(true);
		_displayImage.enabled = false;
	}
}
